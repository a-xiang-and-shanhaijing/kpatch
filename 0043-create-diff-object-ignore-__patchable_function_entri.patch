From b232188e0c220f779ce54955876b56a4161c258a Mon Sep 17 00:00:00 2001
From: Joe Lawrence <joe.lawrence@redhat.com>
Date: Fri, 10 Feb 2023 13:38:05 -0500
Subject: [PATCH 3/3] create-diff-object: ignore __patchable_function_entries

Kernel v6.2+ commit bea75b33895f ("x86/Kconfig: Introduce function
padding") introduces the -fpatchable-function-entry=16,16 build flag on
x86.  This leverages compiler support for generating a
__patchable_function_entries section similar to __mcount_loc.

That said, x86 still utilizes __mcount_loc even when
__patchable_function_entries exists.  The latter point to the __pfx
symbols, but the section is discarded in the vmlinux link and isn't used
regardless, for ftrace or for any other purpose.

Signed-off-by: Joe Lawrence <joe.lawrence@redhat.com>
---
 kpatch-build/create-diff-object.c | 6 ++++++
 1 file changed, 6 insertions(+)

diff --git a/kpatch-build/create-diff-object.c b/kpatch-build/create-diff-object.c
index 8a671bd..0e91513 100644
--- a/kpatch-build/create-diff-object.c
+++ b/kpatch-build/create-diff-object.c
@@ -2885,6 +2885,12 @@ static void kpatch_mark_ignored_sections(struct kpatch_elf *kelf)
 		    !strncmp(sec->name, ".llvm_addrsig", 13) ||
 		    !strncmp(sec->name, ".llvm.", 6))
 			sec->ignore = 1;
+
+		if (kelf->arch == X86_64) {
+			if (!strcmp(sec->name, ".rela__patchable_function_entries") ||
+			    !strcmp(sec->name, "__patchable_function_entries"))
+				sec->ignore = 1;
+		}
 	}
 
 	sec = find_section_by_name(&kelf->sections, ".kpatch.ignore.sections");
-- 
2.27.0

